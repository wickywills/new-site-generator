#!/bin/bash

# Test for installed programs
i=0; n=0; progs=(git vagrant rpl node);
for p in "${progs[@]}"; do
    if hash "$p" &>/dev/null
    then
        echo "$(tput setaf 6)$p is installed$(tput sgr0)"
        sleep .3
        let c++
    else
        echo "$(tput setaf 1)$p is not installed$(tput sgr0)"
        sleep .3
        let n++
    fi
done
sleep .3

printf "$(tput setaf 6)%d of %d programs were installed.\n$(tput sgr0)"  "$i" "${#progs[@]}"
printf "$(tput setaf 6)%d of %d programs are missing\n$(tput sgr0)" "$n" "${#progs[@]}"
sleep 1

if [ $n -gt 0 ]
then
  echo "$(tput setaf 1)Please install missing programs before continuing$(tput sgr0)"
  exit
else
  echo "$(tput setaf 2)All set. Continuing..$(tput sgr0)"
  echo "----------"
fi

# Setup some variables
echo "$(tput setaf 2)What is the name of this site (no special characters, spaces, and all lowercase)?$(tput sgr0)"
read sitename
echo "----------"
echo "$(tput setaf 2)What is your Bitbucket username?$(tput sgr0)"
read bitbucketuser
echo "----------"
echo "$(tput setaf 2)What is your Bitbucket password?$(tput sgr0)"
read -s bitbucketpassword

# Create Bitbucket project

# Create Bitbucket repository
curl -X POST -v -u ${bitbucketuser}:${bitbucketpassword} https://api.bitbucket.org/2.0/repositories/wallmanderco/${sitename} -d '{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks" }'

# Download the boilerplate
echo "----------"
echo "$(tput setaf 2)Cloning boilerplate repository$(tput sgr0)"
git clone https://wallmanderco-jameswills@bitbucket.org/wallmanderco-jameswills/wordpress-boilerplate.git ${sitename}

# Download latest version of WP
cd ${sitename}
echo "----------"
echo "$(tput setaf 2)Downloading the latest version of Wordpress...$(tput sgr0)"
sleep 1
curl -O 'https://wordpress.org/latest.zip'
sleep 1

# Unzip WP
echo "----------"
echo "$(tput setaf 2)Unpacking...$(tput sgr0)"
unzip latest.zip
mv -v wordpress/* .
rm -rf wordpress

# Remove existing themes giving you a clean slate
echo "----------"
echo "$(tput setaf 2)Removing default themes$(tput sgr0)"
rm -rf wp-content/themes/twenty*
sleep 1

# Create wp-config
echo "----------"
echo "$(tput setaf 2)Creating wp-config.php$(tput sgr0)"
cp wp-config-sample.php wp-config.php
rpl "database_name_here" "${sitename}_wp" wp-config.php
rpl "username_here" "${sitename}" wp-config.php
rpl "password_here" "${sitename}123" wp-config.php
sleep 1

# Cleanup
echo "----------"
echo "$(tput setaf 2)Cleaning up$(tput sgr0)"
rm latest.zip
sleep 1

# Finish up
echo "----------"
echo "$(tput setaf 2)All done!$(tput sgr0)"
echo "----------"
sleep 1
echo "$(tput setaf 2)Now log into vagrant (vagrant up, vagrant ssh) and create a database with the following credentials:$(tput sgr0)"
echo "Name: \"${sitename}_wp\""
echo "User: \"${sitename}\""
echo "Password: \"${sitename}123\""
echo "$(tput setaf 2)You can also find this information in your wp-config.php.$(tput sgr0)"
sleep 1
echo "----------"
echo "$(tput setaf 2)Install NPM modules and run gulp with a server? [y,n]$(tput sgr0)"
read input
if [[ $input == "Y" || $input == "y" ]]; then
  cd wp-content/themes/*
  npm install
  gulp
else
  echo "$(tput setaf 2)Ok, then we're done here$(tput sgr0)"
fi
