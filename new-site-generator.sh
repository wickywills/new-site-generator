#!/bin/bash

# Test for installed programs
i=0; n=0; progs=(git vagrant rpl node);
for p in "${progs[@]}"; do
    if hash "$p" &>/dev/null
    then
        echo "$(tput setaf 6)$p is installed$(tput sgr0)"
        sleep .3
        let c++
    else
        echo "$(tput setaf 1)$p is not installed$(tput sgr0)"
        sleep .3
        let n++
    fi
done
sleep .3

printf "$(tput setaf 6)%d of %d programs were installed.\n$(tput sgr0)"  "$i" "${#progs[@]}"
printf "$(tput setaf 6)%d of %d programs are missing\n$(tput sgr0)" "$n" "${#progs[@]}"
sleep 1

if [ $n -gt 0 ]
then
  echo "$(tput setaf 1)Please install missing programs before continuing$(tput sgr0)"
  exit
else
  echo "$(tput setaf 2)All set. Continuing..$(tput sgr0)"
fi
